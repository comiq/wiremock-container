FROM openjdk:8-jre-alpine

ENV WIREMOCK_VERSION 2.16.0

RUN mkdir -p /opt/wiremock/ \
  && wget https://repo1.maven.org/maven2/com/github/tomakehurst/wiremock-standalone/$WIREMOCK_VERSION/wiremock-standalone-$WIREMOCK_VERSION.jar \
  -O /opt/wiremock/wiremock.jar

# We don't want to handle stub files as root so su-exec is used for easy step-down from root
RUN apk add --no-cache 'su-exec>=0.2'

WORKDIR /wiremock

VOLUME ["/wiremock/__files", "/wiremock/mappings"]

EXPOSE 8080 8443

COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["docker-entrypoint.sh"]
